import Form from './Form'
import Grid from './Grid'
import Summary from './Summary'

function App() {
  return (
    <div id='app'>
      <Form />
      <Grid />
      <Summary />
    </div>
  )
}

export default App;
