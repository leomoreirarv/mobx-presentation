import { useState } from 'react'

function Form() {
  const [role, setRole] = useState('');
  const [salary, setSalary] = useState(0);

  const onChangingRole = event => {
    setRole(event.target.value)
  }

  const onChangingSalary = event => {
    setSalary(event.target.value)
  }

  const onSubmit = e => {
    e.preventDefault();
    setRole('');
    setSalary(0);
  }

  return (
    <div id='form'>
      <form>
        <label>Role <input value={role} onChange={onChangingRole} type='text'/></label>
        <label>Salary <input value={salary} onChange={onChangingSalary} type='number'/></label>
        <button onClick={onSubmit}>Save</button>
      </form>
    </div>
  )
}

export default Form;