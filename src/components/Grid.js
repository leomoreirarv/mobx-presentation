function Grid() {
  return (
    <table id='grid'>
      <thead>
        <tr>
          <th>Role</th>
          <th>Salary $</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>...</td>
          <td>...</td>
        </tr>
      </tbody>
    </table> 
  )
}

export default Grid;